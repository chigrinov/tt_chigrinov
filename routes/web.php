<?php


Route::get('/', 'MainController@index');

Route::get('/contacts', 'MainController@contacts');

Route::get('/categories/{category}', 'CategoriesController@index');

Route::get('/product/{id}/{product}', 'ProductsController@show');

Route::get('login', 'UsersController@login');

Route::get('register', 'UsersController@index');

Route::post('/login/auth', 'UsersController@auth');

Route::post('/register/user', 'UsersController@register');

Route::get('/home', 'HomeController@index');

Route::get('/logout', 'UsersController@logout');

Route::get('/search', 'SearchController@search');

Route::get('/admin', 'AdminController@index');

Route::post('/admin/welcome', 'AdminController@auth');

Route::get('/admin/logout', 'AdminController@logout');

Route::get('/admin/categories', 'AdminController@getCategory');

Route::get('/admin/categories/delete/{id}', 'AdminController@deleteCategory');

Route::post('/admin/categories', 'AdminController@addCategory');

Route::get('/admin/sub-categories', 'AdminController@getSubCategory');

Route::get('/admin/sub-categories/delete/{id}', 'AdminController@deleteSubCategory');

Route::post('/admin/sub-categories', 'AdminController@addSubCategory');

Route::get('/admin/products', 'AdminController@indexProducts');

Route::get('/admin/products/{category}', 'AdminController@getProducts');

Route::get('/admin/products/delete/{id}', 'AdminController@deleteProduct');

Route::get('/admin/products/edit/{id}', 'AdminController@editProduct');

Route::post('/admin/product/update/{id}', 'AdminController@updateProduct');

Route::get('/admin/product', 'AdminController@getProduct');

Route::post('/admin/product/add', 'AdminController@addProduct');

Route::get('/ajax-subcat', function(){

	$cat_id = $_GET['cat_id'];

	$sub_categories = \DB::table('sub_categories')->where('category_id', $cat_id)->get();

	return Response::json($sub_categories);

});
