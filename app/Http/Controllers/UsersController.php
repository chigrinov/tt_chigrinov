<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Hash;

class UsersController extends Controller
{

	public function index(){

		return view('auth.register');

	}

	public function login(){

		return view('auth.login');

	}

	public function register(){


		$this->validate(request(), [

			'name' => 'required|min:4',
			'email' => 'required|email',
        	'password' => 'required|min:4|confirmed',
        	'password_confirmation' => 'required|min:4'

			]);	

		$user = \DB::table('users')->where('email', $_POST['email'])->pluck('email');


		if(isset($user[0])){

			return back()->with('message-error', "Такой e-mail уже зарегистрирован!")->withInput();

		}

		else{

			$user = new User;
			$user->name 			= 	$_POST['name'];
			$user->email 			= 	$_POST['email'];
			$user->password 		= 	bcrypt($_POST['password']);
			$user->save();

			Session::flash('message', "Регистрация прошла успешно!");

			return back();

		}

	}

	public function auth(){

        $email = $_POST['email'];
        $pass = $_POST['password'];


        if (Auth::attempt(['email' => $email, 'password' => $pass])){

        	Session::flash('message', 'Авторизация прошла успешно!');

	    	return back();

        }  

        else{

        	Session::flash('message-error', 'Неверная пара логин/пароль!');

	    	return back();

        }

    }


	public function logout(){

		Auth::logout();

		return back();

	}
}