<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;


class ProductsController extends Controller
{

    public function show($id)
    {	
    	// Criteria of popularity
    	Product::where('product_id', $id)->increment('view_count', 1);
    	// End criteria

    	//Get category of product
    	$sub_category = Product::where('product_id', $id)->pluck('sub_category_id');

    	//Get random products from category of product
    	$sameProduct = Product::where('sub_category_id', $sub_category)->inRandomOrder()->limit(4)->get();

    	$products = Product::where('product_id', $id)->get();

		return view('product_by_id', compact('products', 'sameProduct'));
    }
	
}
