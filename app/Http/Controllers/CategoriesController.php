<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;


class CategoriesController extends Controller
{
    public function index($category)
    {	

        $category_id = \DB::table('sub_categories')->where('latin_url', $category)->pluck('sub_category_id');
        
        $adveresting = \DB::table('products')->orderBy('view_count', 'desc')->limit(4)->get();

    	$products = Product::where('sub_category_id', $category_id)->paginate(12);

        $category = \DB::table('sub_categories')->where('latin_url', $category)->pluck('sub_category_title');
        
        $title = $category[0];

    	return view('products', compact('products', 'title', 'adveresting'));
    }
	
}
