<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class SearchController extends Controller
{
	public function search(){

		if(!empty($_GET['keyword']))
	    {

		    $keyword = $_GET['keyword'];

		    $products = Product::where('title', 'LIKE', '%' . $keyword . '%')->paginate(12);


		}
		else{

			return back();
			
		} 

		$adveresting = \DB::table('products')->orderBy('view_count', 'desc')->limit(4)->get();

		return view('products', compact('products', 'adveresting'));
		
	}

}