<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{

	public function index(){

		return view('admin.welcome');

	}

	public function getCategory(){

		$categories = \DB::table('categories')->get();

		return view('admin.categories', compact('categories'));

	}

	public function addCategory(){

		
		\DB::table('categories')->insert(['title' => $_POST['new_category']]);

		return back();

	}

	public function deleteCategory($id){

		\DB::table('categories')->where('category_id', '=', $id)->delete();

		return back();

	}

	public function getSubCategory(){

		$categories = \DB::table('categories')->get();

		return view('admin.sub_categories', compact('categories'));

	}

	public function deleteSubCategory($id){

		\DB::table('sub_categories')->where('sub_category_id', '=', $id)->delete();

		return back();

	}

	public function addSubCategory(){

		$latin_url = $_POST['new_sub_category'];

		function translitIt($str){
		    $tr = array(
		        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d",
		        "Е"=>"e","Ё"=>"yo","Ж"=>"j","З"=>"z","И"=>"i",
		        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
		        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
		        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch",
		        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
		        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
		        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
		        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
		        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
		        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
		        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
		        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
		        " "=> "-", "."=> "", "/"=> "-"
		    );
		    return strtr($str,$tr);
		}

		if (preg_match('/[^A-Za-z0-9_\-]/', $latin_url)) {
		    $latin_url = translitIt($latin_url);
		    $latin_url = preg_replace('/[^A-Za-z0-9_\-]/', '', $latin_url);
		}


		$category_id = \DB::table('categories')->where('title', $_POST['category'])->pluck('category_id');

		\DB::table('sub_categories')->insert([
											'sub_category_title' => $_POST['new_sub_category'],
											'category_id' 		 => $category_id[0],
											'latin_url' 		 => $latin_url
												]);

		return back();

	}

	public function indexProducts(){

		$sub_categories = \DB::table('sub_categories')->get();

		return view('admin.products', compact('sub_categories'));

	}

	public function getProducts($category){

		$sub_category_id = \DB::table('sub_categories')->where('latin_url', $category)->pluck('sub_category_id');

		$products = \DB::table('products')->where('sub_category_id', $sub_category_id[0])->get();

		$sub_categories = \DB::table('sub_categories')->get();

		return view('admin.products', compact('products', 'sub_categories'));

	}

	public function deleteProduct($id){

		\DB::table('products')->where('product_id', $id)->delete();

		return back();

	}

	public function editProduct($id){

		$sub_categories = \DB::table('sub_categories')->get();

		$products = \DB::table('products')->where('product_id', $id)->get();

		return view('admin.edit_product', compact('sub_categories', 'products'));

	}

	public function updateProduct($id){

		\App\Product::where('product_id', $id)
          ->update(['title' 		=> 	$_POST['title'],
          			'description' 	=> 	$_POST['description'],
          			'price' 		=> 	$_POST['price'],
          			'hdd' 			=> 	$_POST['HDD'],
          			'ram' 			=> 	$_POST['RAM'],
          			'cpu' 			=> 	$_POST['CPU'],
          			]);
		
		return back();

	}

	public function getProduct(){

		//$sub_categories = \DB::table('sub_categories')->get();

		$categories = \DB::table('categories')->get();

		return view('admin.add_product', compact('sub_categories', 'categories'));

	}

	public function addProduct(){


		if((!empty($_POST['title'])) && (!empty($_POST['price'])) && (!empty($_POST['sub_category'])) && (is_uploaded_file($_FILES['userfile']['tmp_name']))){       

            function translitIt($str){
			    $tr = array(
			        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d",
			        "Е"=>"e","Ё"=>"yo","Ж"=>"j","З"=>"z","И"=>"i",
			        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
			        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
			        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch",
			        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
			        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
			        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
			        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
			        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
			        " "=> "-", "/"=> "-"
		    	);
		    	return strtr($str,$tr);
			}

			$latin_url = $_POST['title'];

			if (preg_match('/[^A-Za-z0-9_\-]/', $latin_url)) {
			    $latin_url = translitIt($latin_url);
			    $latin_url = preg_replace('/[^A-Za-z0-9_\-]/', '', $latin_url);
			}
			

            $uploaddir = '../public/images/';
            $uploadfile = $uploaddir . translitIt(basename($_FILES['userfile']['name']));


            move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);


            \DB::table('products')->insert([
                ['title'        	=>  $_POST['title'],
                 'description'  	=>  $_POST['description'],
                 'image'        	=>  translitIt($_FILES['userfile']['name']),
                 'price'  			=>  $_POST['price'],
                 'sub_category_id'  =>  $_POST['sub_category'],
                 'latin_url' 		=>  $latin_url,
                 'ram'  =>  $_POST['RAM'],
                 'hdd'  =>  $_POST['HDD'],
                 'cpu'  =>  $_POST['CPU']
                ]
            ]);

            Session::flash('message', 'Товар успешно добавлен!');

            return back();   
        }
        else{

        	return back()->with('message-danger', 'Заполните все поля!')->withInput();

        }
	}

	public function controlSlider(){

		$sliders = \DB::table('sliders')->get();

		return view('admin.control_sliders', compact('sliders'));

	}

	public function activeSlider(){

		\DB::table('sliders')->where('id_slider', $_POST['id_slider'])->update([
				'active'	=>	  $_POST['active']
			]);

		return back();

	}
	

	public function auth(){

        $name = $_POST['admin_name'];
        $pass = $_POST['admin_password'];

        $admin = \DB::table('users')->where('name', $name)->get();



		if(!empty($admin[0])){

	        foreach ($admin as $value){

	        	if (Auth::attempt(['email' => $value->email, 'password' => $pass])){

			    	return view('admin.index');

	        	}
	            else{

	                Session::flash('message', 'Неверная пара логин/пароль!');

	                return back();

	            }  

	        }

    	}

    	else{

            Session::flash('message', 'Неверная пара логин/пароль!');

            return back();

        }  

    }

    public function logout(){

    	Auth::logout();

		return view('admin.welcome');

    }


}