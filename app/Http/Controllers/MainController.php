<?php

namespace App\Http\Controllers;

use App\MainModel;


class MainController extends Controller
{
    public function index()
    {	

        $adveresting = \DB::table('products')->orderBy('view_count', 'desc')->limit(4)->get();

    	$sliders = \DB::table('sliders')->where('active', 1)->get();

    	$count = count($sliders);

		return view('index', compact('sliders', 'count', 'adveresting'));
    
    }
	
}
