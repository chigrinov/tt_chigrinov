-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 05 2017 г., 09:53
-- Версия сервера: 10.1.16-MariaDB
-- Версия PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `online_catalog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`category_id`, `title`, `created_at`) VALUES
(1, 'Элкетроника', '2017-07-02 08:44:24');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `price` float NOT NULL,
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `latin_url` varchar(100) DEFAULT NULL,
  `cpu` varchar(45) DEFAULT NULL,
  `ram` varchar(45) DEFAULT NULL,
  `hdd` varchar(45) DEFAULT NULL,
  `view_count` int(10) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`product_id`, `title`, `description`, `price`, `sub_category_id`, `image`, `latin_url`, `cpu`, `ram`, `hdd`, `view_count`) VALUES
(1, 'Ноутбук Asus X751SV', 'Новый ноутбук серии X отличается от своих предшественников еще более тонким корпусом с красивой отделкой. Современный процессор и многочисленные фирменные технологии от ASUS делают их незаменимыми инструментами для повседневной работы.', 11555, 1, 'asus_x751sv(white).jpg', 'noutbuk-asus-x751sv', '2 ядреный', '4 ГБ', '500 ГБ', 16),
(2, 'Ноутбук Asus Vivobook X556UQ', 'Разработанная специалистами ASUS технология Splendid позволяет быстро настраивать параметры дисплея в соответствии с текущими задачами и условиями, чтобы получить максимально качественное изображение. Она предлагает выбрать один из нескольких предустановленных режимов, каждый из которых оптимизирован под определенные приложения (фильмы, работа с текстом и т.д). В специальном режиме Eye Care реализована фильтрация синей составляющей видимого спектра для повышения комфорта при чтении. Ноутбук оснащен качественной матрицей с разрешением Full HD (1920x1080), которая дает возможность выводить изображение с высоким уровнем детализации.', 20499, 1, 'asus_vivobook_x556uq(dark).jpg', 'noutbuk-asus-vivobook-x556uq', NULL, NULL, NULL, 5),
(3, 'Ноутбук Asus ZenBook UX410UQ', 'Разработанная специалистами ASUS технология Splendid позволяет быстро настраивать параметры дисплея в соответствии с текущими задачами и условиями, чтобы получить максимально качественное изображение. Она предлагает выбрать один из нескольких предустановленных режимов, каждый из которых оптимизирован под определенные приложения (фильмы, работа с текстом и т.д). В специальном режиме Eye Care реализована фильтрация синей составляющей видимого спектра для повышения комфорта при чтении. Ноутбук оснащен качественной матрицей с разрешением Full HD (1920x1080), которая дает возможность выводить изображение с высоким уровнем детализации.', 38169, 1, 'asus_zenbook_ux410uq(quartz_grey).jpg', 'noutbuk-asus-zenbook-ux410uq', NULL, NULL, NULL, NULL),
(4, 'Ноутбук Asus X751SA', 'Новый ноутбук серии X отличается от своих предшественников еще более тонким корпусом с красивой отделкой. Современный процессор и многочисленные фирменные технологии от ASUS делают его незаменимыми инструментами для повседневной работы.', 9869, 1, 'asus_x751sa(white).jpg', 'noutbuk-asus-x751sa', NULL, NULL, NULL, 4),
(5, 'Samsung Galaxy J7 J700H/DS Black', 'С Samsung Galaxy J5 и J7 в вашем распоряжении чрезвычайная функциональность. Наслаждайтесь исключительным качеством изображений и видео с кристально чистым дисплеем Super AMOLED размером 5.0"/5.5", высокой производительностью аппаратных средств и универсальными камерами', 5299, 2, 'samsung_galaxy_j700h(black).jpg', 'samsung-galaxy-j7-j700hds-black', NULL, NULL, NULL, 2),
(6, 'Apple iPhone 5s 16GB Space Gray', 'Процессор A7 с 64-битной архитектурой. Датчик идентификации по отпечатку пальца. Усовершенствованная камера, которая стала ещё быстрее. Операционная система, разработанная специально для 64-битной архитектуры. Любая из этих функций позволила бы смартфону быть на шаг впереди других. А со всеми этими функциями iPhone просто опережает своё время.', 8444, 2, 'apple_iphone_5s_16gb_space(gray).jpg', 'apple-iphone-5s-16gb-space-gray', NULL, NULL, NULL, NULL),
(7, 'Sony Xperia X Dual', '5-дюймовый дисплей обладает отличной яркостью, поэтому вы сможете рассмотреть содержимое экрана не только в условиях низкой освещенности, но и при очень ярком свете. А чтобы детали были четкими, в дисплее используется особая технология улучшения контрастности от Sony.', 9999, 2, 'sony_xperia_x_dual_black.jpg', 'sony-xperia-x-dual', NULL, NULL, NULL, NULL),
(8, 'Lenovo Vibe K5 Note', 'Экран высокого разрешения Full HD с плотностью 403 пиксела на дюйм. Lenovo K5 Note дарит прекрасное качество изображения в играх, видео и фотографиях. Яркий 5.5-дюймовый дисплей обеспечивает широкие углы обзора (178 градусов) и идеально подходит для совместного просмотра видео и фото вместе с друзьями и близкими.', 4555, 2, 'lenovo_vibe_k5_note(silver).jpg', 'lenovo-vibe-k5-note', NULL, NULL, NULL, NULL),
(9, 'Планшет Prestigio MultiPad Visconte 64GB', 'Увеличьте свою производительность с новым 64-битным планшетным ПК Prestigio на базе Windows 8.1 в форме планшета. Prestigio MultiPad Visconte предназначен для активных пользователей, которым нужен единый ультрамобильный компьютер для работы, образования, бизнеса и развлечений.', 4099, 3, 'prestigio_multipad_visconte_64gb.jpg', 'planshet-prestigio-multipad-visconte-64gb', NULL, NULL, NULL, NULL),
(10, 'Планшет Asus Transformer Mini T102HA White', 'Сверхтонкий, сверхлегкий 10.1-дюймовый ASUS Transformer Mini — это сразу два устройства в одном! Созданное в корпусе из алюминиево-магниевого сплава, это ультрапортативное устройство "2-в-1" весит меньше 800 граммов, работает до 11 часов на одном заряде батареи и поддерживает все инновационные функции Windows 10.', 13499, 3, 'asus_transformer_mini_t102ha(white).jpg', 'planshet-asus-transformer-mini-t102ha-white', NULL, NULL, NULL, NULL),
(11, 'Планшет Lenovo Yoga Book YB1-X91L 3G+LTE', 'Мобильность и производительность вступают в новую эру с качественно новыми планшетами "2 в 1". С помощью стилуса с настоящими чернилами вы можете мгновенно создавать записи и преобразовывать их в цифровую форму. Печатайте на клавиатуре Halo, которую можно включить в нужный момент. Тонкий, легкий и стильный, портативный Yoga Book дает свободу вашему воображению. Толщина устройства в сложенном состоянии всего 9.6 мм.', 21999, 3, 'lenovo_yoga_book_b1_x91l.jpg', 'planshet-lenovo-yoga-book-yb1-x91l-3g-lte', NULL, NULL, NULL, NULL),
(12, 'Планшет Toshiba Satellite Click 10 LX0W-C64 64GB Silver', 'Четырехъядерный процессор Intel Atom прекрасно справляется с одновременной обработкой нескольких задач. Его компоненты позволяют управлять приложениями, обеспечивая быструю загрузку и равномерное распределение питания для работы всех программ, запущенных на устройстве. Он делает планшет идеальным многозадачным устройством как для ведения бизнеса, так и для развлечений. Графический процессор, входящий в состав чипа, поддерживает DirectX 11.2, также здесь предусмотрен контроллер памяти LPDDR3-1600. Видеокарта свободно воспроизводит видео 4K/H.265.', 6999, 3, 'toshiba_satelite_click10_lx0-c64.jpg', 'planshet-toshiba-satellite-click-10-lx0w-c64-64gb-silver', NULL, NULL, NULL, NULL),
(13, 'Маршрутизатор TP-LINK TL-WR841ND', 'Маршрутизатор TP-LINK TL-WR841ND разработан для создания сетей для небольших офисов и домов, как с помощью проводного, так и беспроводного подключения.', 499, 4, 'tp_link_tl-wr84nd.jpg', 'marshrutizator-tp-link-tl-wr841nd', NULL, NULL, NULL, NULL),
(14, 'Маршрутизатор Xiaomi Mi WiFi 3c White', NULL, 499, 4, 'xiaomi_mi_wifi_3c.jpg', 'marshrutizator-xiaomi-mi-wifi-3c-white', NULL, NULL, NULL, NULL),
(15, 'Маршрутизатор Asus RT-N12E ', 'Asus RT-N12E_B1 — маршрутизатор предназначенный для использования дома или в небольшом офисе. Он отличается простотой настройкой и использованием и позволяет создать до четырех беспроводных сетей с разными правами доступа и пропускной способностью.', 509, 4, 'asus_rt_n12e_b1.jpg', 'marshrutizator-asus-rt-n12e', NULL, NULL, NULL, NULL),
(16, 'Маршрутизатор TP-LINK TL-WR740N', 'Модель TL-WR740N - это комбинированное устройство для проводного/беспроводного сетевого подключения с возможностями Интернет–маршрутизатора и 4-портовым коммутатором. Беспроводной маршрутизатор серии Lite N поддерживает стандарты 802.11b&g при сохранении совместимости с технологией 802.11n, при этом достигает производительности 802.11n со скоростью передачи данных до 150Мбит/с за более чем приемлемую цену. Скорости передачи данных стандартов 11n и 11g позволяют более оперативно работать с приложениями, требующими высокую скорость передачи данных, например с потоковым видео. Теперь в вашем доме станет возможна высококачественная передача потокового сигнала, VoIP или беспроводное подключение к он-лайн играм, что было практически невозможно при использовании традиционных продуктов стандарта g. Вступайте в новую эру стандарта 11n c беспроводным маршрутизатором серии Lite N!', 399, 4, 'tp_link_tl_wr740n.jpg', 'marshrutizator-tp-link-tl-wr740n', NULL, NULL, NULL, NULL),
(49, 'Acer Aspire ES1-533-C3ZX (NX.GFTEU.004) Black', 'Привлекательный черный дизайн корпуса, текстурированная верхняя крышка, приятная на ощупь, делают этот ноутбук не похожим на другие и придают ему изысканности.', 5990, 1, 'Acer_Aspire_ES1-533-C3ZX.jpg', 'acer-aspire-ES1-533-C3ZX', NULL, NULL, NULL, NULL),
(50, 'Ноутбук Acer Extensa EX2519-C00V', 'Время автономной работы Extensa позволит вам быть в движении и в то же самое время эффективно работать весь день. Получайте доступ к своим файлам с любых устройств, используя AcerCloud. Движение с комфортом: Acer Extensa — это один из самых тонких ноутбуков в своем классе с эргономичным дизайном.', 7099, 1, 'Acer_Extensa_EX2519-C00V.jpg', 'acer-extensa-EX2519-C00V', NULL, NULL, NULL, NULL),
(51, 'Компьютеры и ноутбуки Ноутбуки Другие модели Acer', 'Привлекательный черный дизайн корпуса, текстурированная верхняя крышка, приятная на ощупь, делают этот ноутбук не похожим на другие и придают ему изысканности.', 11999, 1, 'Acer_Aspire_ES1-572-35T5.jpg', 'acer-aspire-ES1-572-35T5', NULL, NULL, NULL, NULL),
(109, 'Asus VivoBook Max', 'Ноутбук ASUS серии VivoBook X отличается великолепными мультимедийными возможностями. Оснащенный мощным процессором Intel, он справится с самыми ресурсоемкими задачами. Эксклюзивная аудиотехнология SonicMaster с программными средствами ICEpower обеспечивает беспрецедентное для мобильных компьютеров качество звучания.', 6993, 1, 'asus_x541sa.jpg', 'Asus-VivoBook-Max', 'Двухъядерный', '8 ГБ', '450 ГБ', NULL),
(111, 'Ноутбук Asus X751SV', 'Новый ноутбук серии X отличается от своих предшественников еще более тонким корпусом с красивой отделкой. Современный процессор и многочисленные фирменные технологии от ASUS делают их незаменимыми инструментами для повседневной работы.', 11555, 1, 'asus_x751sv(white).jpg', 'noutbuk-asus-x751sv', '2 ядреный', '4 ГБ', '500 ГБ', NULL),
(112, 'Ноутбук Asus Vivobook X556UQ', 'Разработанная специалистами ASUS технология Splendid позволяет быстро настраивать параметры дисплея в соответствии с текущими задачами и условиями, чтобы получить максимально качественное изображение. Она предлагает выбрать один из нескольких предустановленных режимов, каждый из которых оптимизирован под определенные приложения (фильмы, работа с текстом и т.д). В специальном режиме Eye Care реализована фильтрация синей составляющей видимого спектра для повышения комфорта при чтении. Ноутбук оснащен качественной матрицей с разрешением Full HD (1920x1080), которая дает возможность выводить изображение с высоким уровнем детализации.', 20499, 1, 'asus_vivobook_x556uq(dark).jpg', 'noutbuk-asus-vivobook-x556uq', NULL, NULL, NULL, NULL),
(113, 'Ноутбук Asus ZenBook UX410UQ', 'Разработанная специалистами ASUS технология Splendid позволяет быстро настраивать параметры дисплея в соответствии с текущими задачами и условиями, чтобы получить максимально качественное изображение. Она предлагает выбрать один из нескольких предустановленных режимов, каждый из которых оптимизирован под определенные приложения (фильмы, работа с текстом и т.д). В специальном режиме Eye Care реализована фильтрация синей составляющей видимого спектра для повышения комфорта при чтении. Ноутбук оснащен качественной матрицей с разрешением Full HD (1920x1080), которая дает возможность выводить изображение с высоким уровнем детализации.', 38169, 1, 'asus_zenbook_ux410uq(quartz_grey).jpg', 'noutbuk-asus-zenbook-ux410uq', NULL, NULL, NULL, NULL),
(114, 'Ноутбук Asus X751SA', 'Новый ноутбук серии X отличается от своих предшественников еще более тонким корпусом с красивой отделкой. Современный процессор и многочисленные фирменные технологии от ASUS делают его незаменимыми инструментами для повседневной работы.', 9869, 1, 'asus_x751sa(white).jpg', 'noutbuk-asus-x751sa', NULL, NULL, NULL, NULL),
(115, 'Ноутбук Asus X751SV', 'Новый ноутбук серии X отличается от своих предшественников еще более тонким корпусом с красивой отделкой. Современный процессор и многочисленные фирменные технологии от ASUS делают их незаменимыми инструментами для повседневной работы.', 11555, 1, 'asus_x751sv(white).jpg', 'noutbuk-asus-x751sv', '2 ядреный', '4 ГБ', '500 ГБ', NULL),
(116, 'Ноутбук Asus Vivobook X556UQ', 'Разработанная специалистами ASUS технология Splendid позволяет быстро настраивать параметры дисплея в соответствии с текущими задачами и условиями, чтобы получить максимально качественное изображение. Она предлагает выбрать один из нескольких предустановленных режимов, каждый из которых оптимизирован под определенные приложения (фильмы, работа с текстом и т.д). В специальном режиме Eye Care реализована фильтрация синей составляющей видимого спектра для повышения комфорта при чтении. Ноутбук оснащен качественной матрицей с разрешением Full HD (1920x1080), которая дает возможность выводить изображение с высоким уровнем детализации.', 20499, 1, 'asus_vivobook_x556uq(dark).jpg', 'noutbuk-asus-vivobook-x556uq', NULL, NULL, NULL, NULL),
(117, 'Ноутбук Asus ZenBook UX410UQ', 'Разработанная специалистами ASUS технология Splendid позволяет быстро настраивать параметры дисплея в соответствии с текущими задачами и условиями, чтобы получить максимально качественное изображение. Она предлагает выбрать один из нескольких предустановленных режимов, каждый из которых оптимизирован под определенные приложения (фильмы, работа с текстом и т.д). В специальном режиме Eye Care реализована фильтрация синей составляющей видимого спектра для повышения комфорта при чтении. Ноутбук оснащен качественной матрицей с разрешением Full HD (1920x1080), которая дает возможность выводить изображение с высоким уровнем детализации.', 38169, 1, 'asus_zenbook_ux410uq(quartz_grey).jpg', 'noutbuk-asus-zenbook-ux410uq', NULL, NULL, NULL, NULL),
(118, 'Ноутбук Asus X751SA', 'Новый ноутбук серии X отличается от своих предшественников еще более тонким корпусом с красивой отделкой. Современный процессор и многочисленные фирменные технологии от ASUS делают его незаменимыми инструментами для повседневной работы.', 9869, 1, 'asus_x751sa(white).jpg', 'noutbuk-asus-x751sa', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `sliders`
--

CREATE TABLE `sliders` (
  `id_slider` int(11) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `title` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sliders`
--

INSERT INTO `sliders` (`id_slider`, `image`, `active`, `title`) VALUES
(1, 'slider1.png', 0, 'Слайдер 1'),
(2, 'slider2.png', 0, 'Слайдер 2'),
(3, 'slider3.png', 0, 'Слайдер 3'),
(6, 'mount.jpg', 1, '1'),
(7, 'keywords.png', 1, '2'),
(8, 'success.png', 1, '3');

-- --------------------------------------------------------

--
-- Структура таблицы `sub_categories`
--

CREATE TABLE `sub_categories` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `sub_category_title` varchar(100) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `latin_url` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sub_categories`
--

INSERT INTO `sub_categories` (`sub_category_id`, `sub_category_title`, `category_id`, `latin_url`) VALUES
(1, 'Ноутбуки', 1, 'noutbuki'),
(2, 'Смартфоны', 1, 'smartfoni'),
(3, 'Планшеты', 1, 'pansheti'),
(4, 'Сетевое оборудование', 1, 'setevoe-oborudovanie');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(70) CHARACTER SET utf8mb4 NOT NULL,
  `password` varchar(60) CHARACTER SET utf8mb4 NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(19, 'admin', 'admin@mail.ru', '$2y$10$vi1y.ZsqfWEMqFxbxE1Ms.nj8jq3x.dteRta4DNUKc131eGKUnyRm', '6Ix1A1eXAwE1kznJOVeqH3Hm3J3iUeuf58Cc7sDAaTtN78hSIRRZgKy2cMB5', '2017-04-23 18:20:42', '2017-04-23 18:20:42'),
(21, 'kirill', 'kirill@gmail.com', '$2y$10$Ngf.97EqQynJdZag4rsxcepOjOX7qrWRxYbhXvkYNvnrBs4KPKdZ.', 'qeInkNXc1Sst9j5xMN04PfHjVDqgHAVE3NQQIg6P3XyKJCDIkFE8PwYqNtUi', '2017-04-24 17:05:34', '2017-04-24 17:05:34');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `sub_category_id` (`sub_category_id`);

--
-- Индексы таблицы `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id_slider`);

--
-- Индексы таблицы `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`sub_category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT для таблицы `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `sub_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_sub_category` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`sub_category_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
