<div class="col-md-11 adveresting">
	<h2>Похожие товары</h2>
	@foreach ($sameProduct as $product)
		<div class="col-md-12" style="text-align: center;">
			<img class="image-adveresting" src="/public/images/{{ $product->image }}">
		</div>
		<h4 class="title"><a href="/product/{{ $product->product_id }}/{{ $product->latin_url }}">{{ $product->title }}</a></h4>
		<p class="price">{{ $product->price }} грн</p>
	@endforeach
</div>