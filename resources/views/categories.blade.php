<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav categories_menu left-bar">


	<?php

		$categories = DB::table('categories')->get();

	?>

	@foreach($categories as $category)

		<li class="dropdown main_category">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ $category->title }}
				<span class="caret arrow"></span>
			</a>

			<?php 

				$sub_categories = DB::table('sub_categories')->where('category_id', $category->category_id)->get();
				
			?>

			<ul class="dropdown-menu">
			
				@foreach($sub_categories as $sub_category)

				<?php
					$title = $sub_category->sub_category_title;
				?>

					<li>
						<a href="/categories/{{ $sub_category->latin_url }}">{{ $sub_category->sub_category_title }}</a>
					</li>

				@endforeach

			</ul>
		</li>		

	@endforeach

	</ul>
</div>	