@extends('layouts.index') 

@section ('products')


@if (Session::has('message'))
	   			<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
@foreach ($products as $product)


	<div class="col-md-4 product">
		<a href="/product/{{ $product->product_id }}/{{ $product->latin_url }}"><img class="image" src="/public/images/{{ $product->image }}"></a>
		<h3 class="title"><a href="/product/{{ $product->product_id }}/{{ $product->latin_url }}">{{ $product->title }}</a></h3>
		<div class="buy_product col-md-12">
			<span class="price">{{ $product->price }} грн</span>
			<a href="/add-to-cart/{{ $product->product_id }}"><img src="/public/images/cart.png" class="buy_button"/></a>
		</div>
	</div>

@endforeach

<div class="col-md-12" style="text-align:center">
	@if(isset($_GET['keyword']))
		{{ $products->appends(['keyword' => $_GET['keyword']])->render() }}
	@else	
		{{ $products->render() }}
	@endif
</div>



<script>
$(".alert-success").alert();
window.setTimeout(function() { $(".alert-success").alert('close'); }, 1500);
</script>
@endsection