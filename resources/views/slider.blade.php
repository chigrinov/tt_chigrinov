
    <div class="col-md-12 col-md-offset-2 slider-width">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!-- Маркеры слайдов -->
        <ol class="carousel-indicators">
        @for ($i = 0; $i <= ($count - 1); $i++)
        @if($i==0)
        <li data-target="#carousel-example-generic" data-slide-to="{{ $i }}" class="active"></li>
        @else
        <li data-target="#carousel-example-generic" data-slide-to="{{ $i }}"></li>
        @endif
        @endfor 
        </ol>

        <!-- Содержимое слайдов -->
        <div class="carousel-inner">
          @foreach($sliders as $key => $slider)
          @if($key == 0)
            <div class="item active"">
              <img src="public/images/{{ $slider->image }}" class="slider-image">
            </div>
          @else
            <div class="item">
              <img src="public/images/{{ $slider->image }}" class="slider-image">
            </div>
          @endif    
          @endforeach 
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
          <img class="left-arrow" src="public/images/arrow_left.png">
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
          <img class="left-arrow" src="public/images/arrow_right.png">
        </a>
        
      </div>
    </div>

