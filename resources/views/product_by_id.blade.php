@extends('layouts.index') 

@section ('product_by_id')


@foreach ($products as $product)
	<?php
		$title= $product->title;
	?>
	<div class="col-md-12 product">
		<div class="col-md-6">
			<img class="image" src="/public/images/{{ $product->image }}">
		</div>
		<div class="col-md-6">	
			<h3 class="title">{{ $product->title }}</a></h3>
			@if (Session::has('message'))
	   			<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
			<p class="price">{{ $product->price }} грн</p>
			<p>{{ $product->description }}</p>
			@if ((isset($product->ram) && $product->ram != '') || (isset($product->hdd) && $product->hdd != '') || (isset($product->cpu) && $product->cpu != ''))
				<h4>Характеристики</h4>
					<ul>
						@if (isset($product->ram) && $product->ram != '')
							<li>Объем оперативной памяти : {{ $product->ram }}</li>
						@endif
						@if (isset($product->hdd) && $product->hdd != '')
							<li>Объём накопителя : {{ $product->hdd }}</li>
						@endif
						@if (isset($product->cpu) && $product->cpu != '')
							<li>Процессор : {{ $product->cpu }}</li>
						@endif
					</ul>
			@endif
			<a href="/add-to-cart/{{ $product->product_id }}"><img src="/public/images/cart.png" class="buy_button"/></a>
		</div>	
	</div>

@endforeach

<script>
$(".alert-success").alert();
window.setTimeout(function() { $(".alert-success").alert('close'); }, 1500);
</script>
@endsection