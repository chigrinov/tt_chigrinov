<form method="get" action="/search">
<div class="container">
	<div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div id="imaginary_container"> 
                <div class="input-group stylish-input-group">

                    <input type="text" class="form-control search"  placeholder="Search" name="keyword">
                    <span class="input-group-addon">
                        <button type="submit" class="btn btn-primary btn-sm">Search</button>
                    </span>
                </div>
            </div>
        </div>
	</div>
</div>
</form>