@extends('layouts.index')

@section ('register')

<?php
    $title = 'Регистрация';
?>


<div class="col-md-6 col-md-offset-3">
	@if (Session::has('message'))
		<div class="alert alert-success">{{ Session::get('message') }}</div>
	@endif
	@if (Session::has('message-error'))
		<div class="alert alert-danger">{{ Session::get('message-error') }}</div>
	@endif
	<form  method="post" action="/register/user">
		{{ csrf_field() }}
		<div class="form-group div-register">
			<label for="name" class="cols-sm-2 control-label label-register">Ваше имя</label>
			<div class="cols-sm-10">
				<div class="form-group">
					<input type="text" class="form-control" name="name" id="name"  value="{{ old('name') }}" placeholder="Введите имя" />
				</div>
			</div>
		</div>

		<div class="form-group div-register">
			<label for="email" class="cols-sm-2 control-label label-register">Ваш Email</label>
			<div class="cols-sm-10">
				<div class="form-group">
					<input type="text" class="form-control" name="email" id="email" value="{{ old('email') }}" placeholder="Введите Email"/>
				</div>
			</div>
		</div>

		<div class="form-group div-register">
			<label for="password" class="cols-sm-2 control-label label-register">Пароль</label>
			<div class="cols-sm-10">
				<div class="form-group">
					<input type="password" class="form-control" name="password" id="password"  placeholder="Введите пароль"/>
				</div>
			</div>
		</div>

		<div class="form-group div-register">
			<label for="confirm" class="cols-sm-2 control-label label-register">Повторите пароль</label>
			<div class="cols-sm-10">
				<div class="form-group">
					<input type="password" class="form-control" name="password_confirmation" id="confirm"  placeholder="Подтвердите пароль"/>
				</div>
			</div>
		</div>

		<div class="form-group ">
			<button type="submit" class="btn btn-primary">Регистрация</button>
		</div>
		
		@include('errors')

	</form>
</div>



@endsection