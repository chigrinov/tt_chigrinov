@extends('layouts.index')

@section ('login')

<?php
    $title = 'Вход в аккаунт';
?>

<div class="col-md-6 col-md-offset-3">

	@if(Session::has('message'))
		<div class="col-md-12 alert alert-success">
			{{ Session::get('message') }}
		</div>
	@elseif(Session::has('message-error'))
		<div class="col-md-12 alert alert-danger">
			{{ Session::get('message-error') }}
		</div>
	@endif

	<form class="" method="post" action="login/auth">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name" class="cols-sm-2 control-label">Ваш Email</label>
			<div class="cols-sm-10">
				<div class="form-group">
					<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

					@if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

				</div>
			</div>
		</div>

		<div class="form-group">
			<label for="email" class="cols-sm-2 control-label">Ваш пароль</label>
			<div class="cols-sm-10">
				<div class="form-group">
					<input id="password" type="password" class="form-control" name="password" required>

					@if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

				</div>
			</div>
		</div>

	
		<div class="form-group ">
			<button type="submit" class="btn btn-primary">Войти</button>
		</div>

	</form>
</div>

@endsection