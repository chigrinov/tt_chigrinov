@extends('admin.index')

@section('sub_categories')


<div class="col-md-6">
	<table class="table">
		<thead>
			<th>Категория</th>
			<th>Подкатегория</th>
			<th>Действие</th>
		</thead>
		<tbody>
			@foreach($categories as $category)
			<tr>
				<?php $sub_categories = DB::table('sub_categories')->where('category_id', $category->category_id)->get(); ?>
				@foreach($sub_categories as $sub_category)
				<td>{{ $category->title }}</td>
				<td>{{ $sub_category->sub_category_title }}</td>
				<td><a href="/admin/sub-categories/delete/{{ $sub_category->sub_category_id }}" onclick="return confirm('Вы уверены?') ? true : false;">Удалить</a></td>
			</tr>
				@endforeach
			@endforeach
		</tbody>
	</table>
</div>

<div class="col-md-6" style="text-align:center">
	<form method="post" action="/admin/sub-categories">
    {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-md-8 col-md-offset-2">
                <label for="new_category" class="h4">Новая подкатегория</label>
                <input type="text" class="form-control" name="new_sub_category" required>
            </div>
            <div class="form-group col-md-8 col-md-offset-2">
            	<label>Выберите категорию</label>
                <select name="category">
                	@foreach($categories as $category)
                	<option>{{ $category->title }}</option>
                	@endforeach
                </select>
            </div>
            <div class="form-group col-md-8 col-md-offset-2">
                <button type="submit" class="form-control btn-success">Создать</button>
            </div>
        </div>
    </form>    
</div>



@endsection