@extends('admin.index')

@section('edit_product')

<div class="col-md-2 admin-menu">
	<table class="table">
		<thead>
			<th>Категория</th>
		</thead>
		<tbody>
		@foreach($sub_categories as $sub_category)
			<tr>
				<td><a href="/admin/products/{{ $sub_category->latin_url }}">{{ $sub_category->sub_category_title }}</a></td>
			</tr>
		@endforeach	
		</tbody>
	</table>
</div>
<div class="col-md-9 col-md-offset-3 admin-products">
	@if(isset($products))
		@foreach ($products as $product)

		<div class="col-md-9 product">
			<form method="post" action="/admin/product/update/{{ $product->product_id }}">
			{{ csrf_field() }}
				<img class="image" src="/public/images/{{ $product->image }}"></br>
				<label for="title">Название</label>
	  			<textarea class="form-control" name="title" rows="1">{{ $product->title }}</textarea></br>
	  			<label for="description">Описание</label>
	  			<textarea class="form-control" name="description" rows="5">{{ $product->description }}</textarea></br>
	  			<label for="price">Цена</label>
	  			<input type="number" class="form-control" name="price" value="{{ $product->price }}"></br>
	  			<label for="HDD">HDD</label>
	  			<input type="text" class="form-control" name="HDD" value="{{ $product->hdd }}"></br>
	  			<label for="CPU">CPU</label>
	  			<input type="text" class="form-control" name="CPU" value="{{ $product->cpu }}"></br>
	  			<label for="RAM">RAM</label>
	  			<input type="text" class="form-control" name="RAM" value="{{ $product->ram }}"></br>
				<button type="submit" class="btn btn-primary">Готово</button>
			</form>	
		</div>

		@endforeach
	@endif
</div>


@endsection
