<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin panel</title>
    <link href="/public/css/app.css" rel="stylesheet" type="text/css">
    <link href="/public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/public/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
</head>
<body>

@if(Auth::user() === null)
    <script type="text/javascript">
    window.location = "{{ url('/admin') }}";
    </script>
@else
    @if(Auth::user()->email !== 'admin@mail.ru')
        <script type="text/javascript">
        window.location = "{{ url('/admin') }}";
        </script>
    @endif    
@endif


<div class = "col-xs-12 col-md-2 admin-menu" style = "background: #333;">
    <ul class="nav">
        <li><a href="/admin/categories">Категории</a></li>
        <li><a href="/admin/sub-categories">Подкатегории</a></li>
        <li><a href="/admin/products">Редактирование товара и Удаление товара</a></li>
        <li><a href="/admin/product">Добавить товар</a></li>
    </ul>
</div>

<div class="col-md-10 col-md-offset-2 auth-admin">
    @if(Auth::user())
        <span>Здравствуйте, {{ Auth::user()->name }}</span>
    @endif
    <a href="/" class="go-to-main">Перейти на главную</a>
    <a href="/admin/logout" class="logout-admin">Выйти</a>
</div>



<div class = "col-xs-12 col-md-10 col-md-offset-2">
    @yield('categories')
    @yield('sub_categories')
    @yield('products')
    @yield('edit_product')
    @yield('add_product')
</div>

</body>
</html>