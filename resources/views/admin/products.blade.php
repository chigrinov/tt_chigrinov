@extends('admin.index')

@section('products')


<div class="col-md-3">
	<ul class="nav">


	<?php

		$categories = DB::table('categories')->get();

	?>

	@foreach($categories as $category)

		<li class="dropdown main_category">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ $category->title }}
				<span class="caret arrow"></span>
			</a>

			<?php 

				$sub_categories = DB::table('sub_categories')->where('category_id', $category->category_id)->get();
				
			?>

			<ul class="dropdown-menu">
			
				@foreach($sub_categories as $sub_category)

				<?php
					$title = $sub_category->sub_category_title;
				?>

					<li>
						<a href="/admin/products/{{ $sub_category->latin_url }}">{{ $sub_category->sub_category_title }}</a>
					</li>

				@endforeach

			</ul>
		</li>		

	@endforeach

	</ul>
</div>

<div class="col-md-8 admin-products">
	@if(isset($products))
		@foreach ($products as $product)

		<div class="col-md-6 admin-product">
			<img class="image" src="/public/images/{{ $product->image }}">
			<h3 class="title">{{ $product->title }}</h3>
			<span class="price">{{ $product->price }} грн</span>
			<span class="change-product"><a href="edit/{{ $product->product_id }}"><button type="button" class="btn btn-primary">Редактировать</button></a></span>
			<span class="delete-product"><a href="delete/{{ $product->product_id }}" onclick="return confirm('Вы уверены?') ? true : false;"><button type="button" class="btn btn-primary">Удалить</button></a></span>
		</div>

		@endforeach
	@endif
</div>


@endsection