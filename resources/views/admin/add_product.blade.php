@extends('admin.index')

@section('add_product')


<div class="col-md-12" style="text-align:center">
	<div class="form-group col-md-8 col-md-offset-2">
		@if(Session::has('message'))
			<div class="alert alert-success">{{ Session::get('message') }}</div>
		@endif
		@if(Session::has('message-danger'))
			<div class="alert alert-danger">{{ Session::get('message-danger') }}</div>
		@endif
	</div>
	<form method="post" action="/admin/product/add" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-md-3 col-md-offset-2">
                <label for="title" class="h4">Название</label>
                <input type="text" class="form-control" name="title" value="" required>
            </div>
            <div class="form-group col-md-2">
                <label for="price" class="h4">Цена</label>
                <input type="number" class="form-control" name="price" value="" required>
            </div>

            <div class="form-group col-md-3">
                <label for="HDD" class="h4">HDD</label>
                <input type="text" class="form-control" name="HDD" value="">
            </div>

            <div class="form-group col-md-2 col-md-offset-2">
                <label for="CPU" class="h4">CPU</label>
                <input type="text" class="form-control" name="CPU" value="">
            </div>

            <div class="form-group col-md-2">
                <label for="RAM" class="h4">RAM</label>
                <input type="text" class="form-control" name="RAM" value="">
            </div>

            <form method="get" action="">
            {{ csrf_field() }}

                <div class="form-group col-md-2">
                    <label for="category" class="h4">Категория</label>
                    <select name="category"  id="category" class="form-control">
                        <option value=""></option>
                        @foreach($categories as $category)
                        <option value="{{ $category->category_id }}">
                            {{ $category->title }}
                        </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-2">
                    <label for="sub_category" class="h4">Подкатегория</label>
                    <select name="sub_category" id="sub_category" class="form-control">
                        <option value="">
                        </option>
                    </select>
                </div>

            </form>

            <script>
                
                $('#category').on('change', function(e){

                    var cat_id = e.target.value;

                    $.get('/ajax-subcat?cat_id=' + cat_id, function(data){
                        
                        $('#sub_category').empty();

                        $.each(data, function(index, subcatObj){

                            $('#sub_category').append('<option value="' + subcatObj.sub_category_id + '">' + subcatObj.sub_category_title + '</option>');

                        });

                    });
                });

            </script>


            <div class="form-group col-md-8 col-md-offset-2">
                <label for="description" class="h4">Описание</label>
                <textarea type="text" class="form-control" name="description" rows="5"></textarea>
            </div>
            <div class="form-group col-md-8 col-md-offset-2">
		      <input type="hidden" name="MAX_FILE_SIZE" value="30000000" />
		      <label for="new-category" class="col-sm-6 col-form-label">Фото товара :</label>
		      <div class="col-sm-6">
		        <input name="userfile" type="file" accept="image/jpeg,image/png,image/gif" />
		      </div>
		    </div>
            <div class="form-group col-md-8 col-md-offset-2">
                <button type="submit" class="form-control btn-success">Создать</button>
            </div>
        </div>
    </form>    
</div>









<script>
$(".alert-success").alert();
window.setTimeout(function() { $(".alert-success").alert('close'); }, 2000);
</script>

@endsection