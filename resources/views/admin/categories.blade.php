@extends('admin.index')

@section('categories')


<div class="col-md-6">
	<table class="table">
		<thead>
			<th>Категория</th>
			<th>Дата создания</th>
			<th>Действие</th>
		</thead>
		<tbody>
		@foreach($categories as $category)
			<tr>
				<td>{{ $category->title }}</td>
				<td>{{ $category->created_at }}</td>
				<td><a href="/admin/categories/delete/{{ $category->category_id }}" onclick="return confirm('Вы уверены?') ? true : false;">Удалить</a></td>
			</tr>
		@endforeach	
		</tbody>
	</table>
</div>
<div class="col-md-6" style="text-align:center">
	<form method="post" action="/admin/categories">
    {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-md-8 col-md-offset-2">
                <label for="new_category" class="h4">Новая категория</label>
                <input type="text" class="form-control" name="new_category" required>
            </div>
            <div class="form-group col-md-8 col-md-offset-2">
                <button type="submit" class="form-control btn-success">Создать</button>
            </div>
        </div>
    </form>    
</div>


@endsection