<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

      <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Internet shop</a>
      </div>

      <div class="collapse navbar-collapse" style="margin-top: 15px;">

          @if(session_status() !== PHP_SESSION_ACTIVE)
            <?php session_start(); ?>
          @endif
  
          @if(Auth::user()) 
            <div class="nav navbar-nav navbar-right register">
              <span class="login">Здравствуйте, {{ Auth::user()->name }}</span>
              <a href="/logout">Выйти</a>
            </div>
          @else
            <div class="nav navbar-nav navbar-right register">
              <a href="/login" class="login">Войти</a>
              <a href="/register">Регистрация</a>
            </div>
          @endif
      </div>

    </div>
</nav>