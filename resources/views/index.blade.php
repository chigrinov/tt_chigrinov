<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@if(isset($title)){{ $title }}
@else
Online catalog
@endif
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/public/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
</head>
<body>




<div class="container main-content">
    @include('nav_bar')
    @include ('search')
    @include ('categories')

    <div class="col-sm-6 col-sm-offset-1 main">
    	@include('slider')
    </div>
    <div class="col-sm-3" style="float: right;">
        @include('advertising')
    </div>
</div>



</body>
</html>