<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@if(isset($title)){{ $title }}
@else
Online catalog
@endif
    </title>
    <link href="/public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/public/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	@include('nav_bar')
	@include ('search')
	@include ('categories')

	<div class="col-sm-7 main">
		@yield('products')
		@yield('product_by_id')
		@yield('register')
		@yield('login')
	</div>
	<div class="col-sm-2" style="float: right;">
		@if(isset($adveresting))
			@include('advertising')
		@endif
		@if(isset($sameProduct))
			@include('same_product')
		@endif	
	</div>
</div>

</body>
</html>